import flask
from flask import request, jsonify
from flask_cors import CORS, cross_origin
import sqlite3
import pytz
import datetime
import time
import calendar
import collections
import re


#####################################################
##						Vars 
#####################################################
app = flask.Flask(__name__)
app.config["DEBUG"] = True
tz = pytz.timezone("America/Los_Angeles")
dataBase = '/Volumes/sambashare/weather_station.db'
#dataBase = '_db/weather_station.db'
apiDoc = "apiDoc.txt"

def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d

#####################################################
##						Routes
#####################################################
@app.route('/', methods=['GET'])
def home():
	return '''<h1>Distant Reading Archive</h1>
<p>A prototype API for distant reading of science fiction novels.</p>'''

@app.route('/api/v1/resources/docs', methods=['GET'])
def api_doc():
	file = open(apiDoc, 'rb') 
	text = file.read()
	file.close()
	return(text)

@app.route('/api/ws/last_10', methods=['GET'])
def weather_station_last_ten():
	conn = sqlite3.connect(dataBase)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_entries= cur.execute('SELECT * FROM weatherTracker ORDER BY "id" DESC LIMIT 10;').fetchall()
	all_entries.insert(0,{'Number of Results' : len(all_entries)})		# Add Number of results as the first entry
	return jsonify(all_entries)
	
@app.route('/api/ws/all', methods=['GET'])
def weather_station_all():
	conn = sqlite3.connect(dataBase)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_entries= cur.execute('SELECT * FROM weatherTracker ORDER BY "id" ASC;').fetchall()
	all_entries.insert(0,{'Number of Results' : len(all_entries)})		# Add Number of results as the first entry
	return jsonify(all_entries)


@app.route('/api/v1/resources/passes/all', methods=['GET'])
def api_passes_all():
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_passes = cur.execute('SELECT * FROM trackerData WHERE resultType=\'%s\';' %('PASS')).fetchall()
	return jsonify(all_passes)

@app.route('/api/v1/resources/count', methods=['GET'])
def api_count_all():
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	#query = "SELECT COUNT(serial) FROM trackerData
	all_passes = cur.execute('SELECT COUNT(serial) FROM trackerData').fetchall()
	return jsonify(all_passes)

@app.route('/api/v1/resources/dups', methods=['GET'])
def api_get_dups():
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	#query = "SELECT COUNT(serial) FROM trackerData
	all_passes = cur.execute('SELECT serial, dgqcKey FROM trackerData GROUP BY dgqcKey HAVING count(*) > 1;').fetchall()
	return jsonify(all_passes)

app.run(host='0.0.0.0')
