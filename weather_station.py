from gpiozero import Button
import time
import math
import tempSensor
import wind_direction_byo
import statistics
import ds18b20_therm
import datetime
#import database
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DateTime, Float
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.ext.declarative import declarative_base
import sqlite3

#####################################################
##				Database Stuff 
#####################################################
engine = create_engine('sqlite:///_db/weather_station.db')
#engine = create_engine('sqlite:////home/pi/Scripts/weather_station/_db/weather_station.db')
Base = declarative_base(bind=engine)

class Weather(Base):
	__tablename__ = 'weatherTracker'
	id 				= Column(Integer, primary_key=True)
	wind_average_deg  	= Column(Float(), nullable=False)
	wind_average_dir  	= Column(String(2), nullable=True, default='')
	wind_speed_kph 		= Column(Float(), nullable=False)
	wind_speed_mph 		= Column(Float(), nullable=False)
	wind_gust_kph 		= Column(Float(), nullable=False)
	wind_gust_mph 		= Column(Float(), nullable=False)
	rainfall_mm 		= Column(Float(), nullable=False)
	rainfall_in 		= Column(Float(), nullable=False)
	humidity_RA 		= Column(Float(), nullable=False)
	pressure_hPA		= Column(Float(), nullable=False)
	pressure_inHG		= Column(Float(), nullable=False)
	ambient_temp_c 	= Column(Float(), nullable=False)
	ground_temp_c 	= Column(Float(), nullable=False)
	ambient_temp_f 	= Column(Float(), nullable=False)
	ground_temp_f 	= Column(Float(), nullable=False)
	dateAdded_utc 	= Column(String(30), default=datetime.datetime.utcnow)
	dateAdded_local = Column(String(30), default=datetime.datetime.now)
	def __init__(self, wind_average_deg, wind_average_dir, wind_speed_kph, wind_speed_mph, wind_gust_kph, wind_gust_mph, rainfall_mm, 
					rainfall_in, humidity_RA, pressure_hPA, pressure_inHG, ambient_temp_c, ambient_temp_f, ground_temp_c,ground_temp_f):
		self.wind_average_deg 	= wind_average_deg
		self.wind_average_dir        = wind_average_dir
		self.wind_speed_kph 	= wind_speed_kph
		self.wind_speed_mph 	= wind_speed_mph
		self.wind_gust_kph 		= wind_gust_kph
		self.wind_gust_mph 		= wind_gust_mph
		self.rainfall_mm 		= rainfall_mm
		self.rainfall_in 		= rainfall_in
		self.humidity_RA 		    = humidity_RA
		self.pressure_hPA 		= pressure_hPA
		self.pressure_inHG = pressure_inHG
		self.ambient_temp_c 	= ambient_temp_c
		self.ambient_temp_f 	= ambient_temp_f
		self.ground_temp_c 	= ground_temp_c
		self.ground_temp_f 	= ground_temp_f

Base.metadata.create_all()
dbSession = sessionmaker(bind=engine)
dbs = dbSession()
#dbs =scoped_session(dbSession)

#####################################################
##					Variables 
#####################################################
wind_count = 0       	# Counts how many half-rotations
radius_cm = 9.0 		# Radius of your anemometer
wind_interval = 5 		# How often (secs) to sample speed
interval =  300 		# measurements recorded every 5 minutes
CM_IN_A_KM = 100000.0	# CM to KM
SECS_IN_AN_HOUR = 3600	# Number of seconds in an hour
ADJUSTMENT = 1.18
KPH_TO_MPH	= 0.62137119223738
BUCKET_SIZE_MM = 0.2794	# Rain per bucket tip in mm
BUCKET_SIZE_IN = 0.011
rain_count = 0
gust = 0
store_speeds = []
store_directions = []

wind_average_dir = ''

#6.215040397762585

#####################################################
##					Conversions to US
#####################################################
def convert_wind(wind_kph):
	wind_mph = (wind_kph * KPH_TO_MPH)
	return wind_mph

def convert_rain(rain_mm):
	rain_in = (rain_mm / 25.4)
	return rain_in

def convert_pressure(pressure_hPA):
	pressure_inhg = (pressure_hPA * 0.02953)
	return pressure_inhg

def convert_temp(temp_c):
	temp_f =((temp_c * (9/5)) + 32)
	return temp_f

#####################################################
##					Functions
#####################################################
# Every half-rotation, add 1 to count
def spin():
	global wind_count
	wind_count = wind_count + 1

def calculate_speed(time_sec):
	global wind_count
	global gust
	circumference_cm = (2 * math.pi) * radius_cm
	rotations = wind_count / 2.0

	# Calculate distance travelled by a cup in km
	dist_km = (circumference_cm * rotations) / CM_IN_A_KM

	# Speed = distance / time
	km_per_sec = dist_km / time_sec
	km_per_hour = km_per_sec * SECS_IN_AN_HOUR

	# Calculate speed
	final_speed = km_per_hour * ADJUSTMENT

	return final_speed

def bucket_tipped():
	global rain_count
	rain_count = rain_count + 1

def reset_rainfall():
	global rain_count
	rain_count = 0

def reset_wind():
	global wind_count
	wind_count = 0

def reset_gust():
	global gust
	gust = 0


#####################################################
##					Sensors
#####################################################
rain_sensor = Button(6)
rain_sensor.when_pressed = bucket_tipped
wind_speed_sensor = Button(5)
wind_speed_sensor.when_activated = spin
temp_probe = ds18b20_therm.DS18B20()


#####################################################
##						Main
#####################################################
while True:
	start_time = time.time()
	while time.time() - start_time <= interval:
		wind_start_time = time.time()
		reset_wind()
		while time.time() - wind_start_time <= wind_interval:
				store_directions.append(wind_direction_byo.get_value())

		final_speed = calculate_speed(wind_interval)# Add this speed to the list
		store_speeds.append(final_speed)
	wind_average_deg = wind_direction_byo.get_average(store_directions)
	wind_gust_kph = max(store_speeds)
	wind_speed_kph = statistics.mean(store_speeds)
	rainfall_mm = rain_count * BUCKET_SIZE_MM
	rainfall_in = rain_count * BUCKET_SIZE_IN
	reset_rainfall()
	store_speeds = []
	store_directions = []
	ground_temp_c = temp_probe.read_temp()
	humidity_RA, pressure_hPA, ambient_temp_c = tempSensor.read_all()

	# print(wind_average, wind_speed, wind_gust, rainfall,  humidity, pressure, ambient_temp, ground_temp)
	# unit = Weather(wind_average, wind_speed, wind_gust, rainfall,  humidity, pressure, ambient_temp, ground_temp)
	# dblock.acquire()

	# Do some conversions
	wind_speed_mph = convert_wind(wind_speed_kph)
	wind_gust_mph = convert_wind(wind_gust_kph)
	pressure_inHG = convert_pressure(pressure_hPA)
	ambient_temp_f = convert_temp(ambient_temp_c)
	ground_temp_f = convert_temp(ground_temp_c)

	unit = Weather(wind_average_deg, wind_average_dir, wind_speed_kph, wind_speed_mph, wind_gust_kph, wind_gust_mph, rainfall_mm, rainfall_in, humidity_RA, pressure_hPA, pressure_inHG, ambient_temp_c, ambient_temp_f, ground_temp_c,ground_temp_f)
	print(wind_average_deg, wind_average_dir, wind_speed_kph, wind_speed_mph, wind_gust_kph, wind_gust_mph, rainfall_mm, rainfall_in, humidity_RA, pressure_hPA, pressure_inHG, ambient_temp_c, ambient_temp_f, ground_temp_c,ground_temp_f)
	dbs.add(unit)
	dbs.commit()
	# dblock.release()
