import time
import math
import datetime
from flask import jsonify
#import database
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DateTime, Float
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.ext.declarative import declarative_base
import sqlite3



dataBase = 'weather_station.db'

def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d




conn = sqlite3.connect(dataBase)
conn.row_factory = dict_factory
cur = conn.cursor()
all_entries= cur.execute('SELECT * FROM weatherTracker ORDER BY "id" ASC;').fetchall()
#all_entries.insert(0,{'Number of Results' : len(all_entries)})		# Add Number of results as the first entry
# old_results = jsonify(all_entries)

#1 hpa to inhg = 0.02953 inhg

# KPH -> MPH (divide the speed value by 1.609)
def convert_wind(wind_kph):
    wind_mph = (wind_kph / 1.609)
    return wind_mph

def convert_rain(rain_mm):
    rain_in = (rain_mm / 25.4)
    return rain_in

def convert_pressure(pressure_hPA):
    pressure_inhg = (pressure_hPA * 0.02953)
    return pressure_inhg

def convert_temp(temp_c):
    temp_f =((temp_c * (9/5)) + 32)
    return temp_f



#####################################################
##				Database Stuff 
#####################################################
engine = create_engine('sqlite:///weather_station_new.db')
Base = declarative_base(bind=engine)

class Weather(Base):
    __tablename__ = 'weatherTracker'
    id 				= Column(Integer, primary_key=True)
    wind_average_deg  	= Column(Float(), nullable=False)
    wind_average_dir  	= Column(String(2), nullable=True, default='')
    wind_speed_kph 		= Column(Float(), nullable=False)
    wind_speed_mph 		= Column(Float(), nullable=False)
    wind_gust_kph 		= Column(Float(), nullable=False)
    wind_gust_mph 		= Column(Float(), nullable=False)
    rainfall_mm 		= Column(Float(), nullable=False)
    rainfall_in 		= Column(Float(), nullable=False)
    humidity_RA 		= Column(Float(), nullable=False)
    pressure_hPA		= Column(Float(), nullable=False)
    pressure_inHG		= Column(Float(), nullable=False)
    ambient_temp_c 	= Column(Float(), nullable=False)
    ground_temp_c 	= Column(Float(), nullable=False)
    ambient_temp_f 	= Column(Float(), nullable=False)
    ground_temp_f 	= Column(Float(), nullable=False)
    # dateAdded_utc 	= Column(String(30), default=datetime.datetime.utcnow)
    # dateAdded_local = Column(String(30), default=datetime.datetime.now)
    dateAdded_utc 	= Column(String(30), nullable=True)
    dateAdded_local = Column(String(30), nullable=True)
    def __init__(self, wind_average_deg, wind_average_dir, wind_speed_kph, wind_speed_mph, wind_gust_kph, wind_gust_mph, rainfall_mm, 
                    rainfall_in, humidity_RA, pressure_hPA, pressure_inHG, ambient_temp_c, ambient_temp_f, ground_temp_c,ground_temp_f,dateAdded_utc,dateAdded_local):
        self.wind_average_deg 	= wind_average_deg
        self.wind_average_dir        = wind_average_dir
        self.wind_speed_kph 	= wind_speed_kph
        self.wind_speed_mph 	= wind_speed_mph
        self.wind_gust_kph 		= wind_gust_kph
        self.wind_gust_mph 		= wind_gust_mph
        self.rainfall_mm 		= rainfall_mm
        self.rainfall_in 		= rainfall_in
        self.humidity_RA 		    = humidity_RA
        self.pressure_hPA 		= pressure_hPA
        self.pressure_inHG = pressure_inHG
        self.ambient_temp_c 	= ambient_temp_c
        self.ambient_temp_f 	= ambient_temp_f
        self.ground_temp_c 	= ground_temp_c
        self.ground_temp_f 	= ground_temp_f
        self.dateAdded_utc  = dateAdded_utc
        self.dateAdded_local  = dateAdded_local
        

Base.metadata.create_all()
dbSession = sessionmaker(bind=engine)
dbs = dbSession()


for item in all_entries:
    wind_average_deg    = item['wind_average']
    wind_average_dir    = ''
    wind_speed_kph      = item['wind_speed']
    wind_speed_mph      = convert_wind(wind_speed_kph)
    wind_gust_kph       = item['wind_gust']
    wind_gust_mph       = convert_wind(wind_gust_kph)
    rainfall_mm         = item['rainfall']
    rainfall_in         = convert_rain(rainfall_mm)
    humidity_RA            = item['humidity']
    pressure_hPA        = item['pressure']
    pressure_inHG       = convert_pressure(pressure_hPA)
    ambient_temp_c      = item['ambient_temp']
    ambient_temp_f      = convert_temp(ambient_temp_c)
    ground_temp_c       = item['ground_temp']
    ground_temp_f      = convert_temp(ground_temp_c)
    dateAdded_utc       = item['dateAdded_utc']
    dateAdded_local     = item['dateAdded_local']
    print(item['wind_average'])



    unit = Weather(wind_average_deg, wind_average_dir, wind_speed_kph, wind_speed_mph, wind_gust_kph, wind_gust_mph, rainfall_mm, rainfall_in, humidity_RA, pressure_hPA, pressure_inHG, ambient_temp_c, ambient_temp_f, ground_temp_c,ground_temp_f,dateAdded_utc,dateAdded_local)
    dbs.add(unit)
    dbs.commit()

'''
class Weather(Base):
	__tablename__ = 'weatherTracker'
	id 				= Column(Integer, primary_key=True)
	wind_average_deg  	= Column(Float(), nullable=False)
    wind_average_dir  	= Column(String(2), nullable=True)
	wind_speed_kph 		= Column(Float(), nullable=False)
	wind_gust_kph 		= Column(Float(), nullable=False)
	rainfall_mm 		= Column(Float(), nullable=False)
	humidity_RA 		= Column(Float(), nullable=False)
	pressure_hPA		= Column(Float(), nullable=False)
	ambient_temp_c 	= Column(Float(), nullable=False)
	ground_temp_c 	= Column(Float(), nullable=False)
	dateAdded_utc 	= Column(String(30), default=datetime.datetime.utcnow)
	dateAdded_local = Column(String(30), default=datetime.datetime.now)
	def __init__(self, wind_average, wind_speed, wind_gust, rainfall,  humidity, pressure, ambient_temp, ground_temp):
		self.wind_average 	= wind_average
		self.wind_speed 	= wind_speed
		self.wind_gust 		= wind_gust
		self.rainfall 		= rainfall
		self.humidity 		= humidity
		self.pressure 		= pressure
		self.ambient_temp 	= ambient_temp
		self.ground_temp 	= ground_temp
'''