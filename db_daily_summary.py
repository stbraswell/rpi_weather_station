#!/usr/bin/python3

import time
import math
import datetime
from flask import jsonify
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DateTime, Float
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.ext.declarative import declarative_base
import sqlite3
import collections
from statistics import mean
import pdb

# MAC
# dataBase_org = 'weather_station_2.db'
# dataBase_daily = 'ws_daily_summary.db'

# NUC
dataBase_org = '/home/troy/sambashare/weather_station.db'
dataBase_daily = '/home/troy/sambashare/ws_daily_summary.db'

def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d

conn = sqlite3.connect(dataBase_org)
conn.row_factory = dict_factory
cur = conn.cursor()

# Get Last day's info
lastDate = cur.execute("SELECT dateAdded_local from weatherTracker WHERE id=(SELECT MAX(id) FROM weatherTracker);")
lastDate = lastDate.fetchone()['dateAdded_local'].split()[0]
lastDate_date = datetime.datetime.strptime(lastDate,'%Y-%m-%d')
yesterday = lastDate_date - datetime.timedelta(days=1)
yesterday_str = str(yesterday).split()[0]
all_entries= cur.execute(f'SELECT * FROM weatherTracker WHERE dateAdded_local = "{yesterday_str}" ORDER BY "id" ASC;').fetchall()

# Get yesterday
all_entries= cur.execute(f'SELECT * FROM weatherTracker WHERE dateAdded_local between "{yesterday_str}" and "{yesterday_str} 23:59:59"ORDER BY "id" ASC;').fetchall()

# All info except for last day
# all_entries= cur.execute(f'SELECT * FROM weatherTracker WHERE dateAdded_local < "{lastDate}" ORDER BY "id" ASC;').fetchall()

# Get all Days Info
# all_entries= cur.execute('SELECT * FROM weatherTracker ORDER BY "id" ASC;').fetchall()



#####################################################
##				Database Stuff 
#####################################################
engine = create_engine('sqlite:////home/troy/sambashare/ws_daily_summary.db')
Base = declarative_base(bind=engine)

class WeatherSum(Base):
    __tablename__ = 'daily_weather_summary'
    id 				        = Column(Integer, primary_key=True)
    sum_day                 = Column(String(30), nullable=False)
    sum_temp_high_c         = Column(Float(), nullable=False)
    sum_temp_low_c          = Column(Float(), nullable=False)
    sum_temp_high_f         = Column(Float(), nullable=False)
    sum_temp_low_f          = Column(Float(), nullable=False)
    sum_gnd_temp_high_c     = Column(Float(), nullable=False)
    sum_gnd_temp_low_c      = Column(Float(), nullable=False)
    sum_gnd_temp_high_f     = Column(Float(), nullable=False)
    sum_gnd_temp_low_f      = Column(Float(), nullable=False)
    sum_wind_gust_avg_kph   = Column(Float(), nullable=False)
    sum_wind_gust_avg_mph   = Column(Float(), nullable=False)
    sum_wind_gust_hi_kph    = Column(Float(), nullable=False)
    sum_wind_gust_hi_mph    = Column(Float(), nullable=False)
    sum_wind_speed_avg_kph  = Column(Float(), nullable=False)
    sum_wind_speed_avg_mph  = Column(Float(), nullable=False)
    sum_wind_dir_avg_deg    = Column(Float(), nullable=False)
    sum_wind_dir_avg_str  	= Column(String(2), nullable=True, default='')
    sum_rain_total_mm       = Column(Float(), nullable=False)
    sum_rain_total_in       = Column(Float(), nullable=False)
    sum_pres_hi_hPA         = Column(Float(), nullable=False)
    sum_pres_lo_hPA         = Column(Float(), nullable=False)
    sum_pres_hi_inHG        = Column(Float(), nullable=False)
    sum_pres_lo_inHG        = Column(Float(), nullable=False)
    sum_pres_avg_inHG       = Column(Float(), nullable=False)
    sum_humidity_hi_RA      = Column(Float(), nullable=False)
    sum_humidity_lo_RA      = Column(Float(), nullable=False)
    sum_humidity_avg_RA     = Column(Float(), nullable=False)


    def __init__(self, sum_day,sum_temp_high_c,sum_temp_low_c,sum_temp_high_f,sum_temp_low_f,sum_gnd_temp_high_c,
                sum_gnd_temp_low_c,sum_gnd_temp_high_f,sum_gnd_temp_low_f,sum_wind_gust_avg_kph,sum_wind_gust_avg_mph,
                sum_wind_gust_hi_kph,sum_wind_gust_hi_mph,sum_wind_speed_avg_kph,sum_wind_speed_avg_mph,sum_wind_dir_avg_deg,
                sum_wind_dir_avg_str,sum_rain_total_mm,sum_rain_total_in,sum_pres_hi_hPA,sum_pres_lo_hPA,sum_pres_hi_inHG,
                sum_pres_lo_inHG,sum_pres_avg_inHG,sum_humidity_hi_RA,sum_humidity_lo_RA,sum_humidity_avg_RA):
        self.sum_day = sum_day
        self.sum_temp_high_c = sum_temp_high_c
        self.sum_temp_low_c = sum_temp_low_c
        self.sum_temp_high_f = sum_temp_high_f
        self.sum_temp_low_f = sum_temp_low_f
        self.sum_gnd_temp_high_c = sum_gnd_temp_high_c
        self.sum_gnd_temp_low_c = sum_gnd_temp_low_c
        self.sum_gnd_temp_high_f = sum_gnd_temp_high_f
        self.sum_gnd_temp_low_f = sum_gnd_temp_low_f
        self.sum_wind_gust_avg_kph = sum_wind_gust_avg_kph
        self.sum_wind_gust_avg_mph = sum_wind_gust_avg_mph
        self.sum_wind_gust_hi_kph = sum_wind_gust_hi_kph
        self.sum_wind_gust_hi_mph = sum_wind_gust_hi_mph
        self.sum_wind_speed_avg_kph = sum_wind_speed_avg_kph
        self.sum_wind_speed_avg_mph = sum_wind_speed_avg_mph
        self.sum_wind_dir_avg_deg = sum_wind_dir_avg_deg
        self.sum_wind_dir_avg_str = sum_wind_dir_avg_str
        self.sum_rain_total_mm = sum_rain_total_mm
        self.sum_rain_total_in = sum_rain_total_in
        self.sum_pres_hi_hPA = sum_pres_hi_hPA
        self.sum_pres_lo_hPA = sum_pres_lo_hPA
        self.sum_pres_hi_inHG = sum_pres_hi_inHG
        self.sum_pres_lo_inHG = sum_pres_lo_inHG
        self.sum_pres_avg_inHG = sum_pres_avg_inHG
        self.sum_humidity_hi_RA = sum_humidity_hi_RA
        self.sum_humidity_lo_RA = sum_humidity_lo_RA
        self.sum_humidity_avg_RA = sum_humidity_avg_RA
        

Base.metadata.create_all()
dbSession = sessionmaker(bind=engine)
dbs = dbSession()

myDates = collections.defaultdict(list)
myDatesSum = collections.defaultdict(list)

for item in all_entries:

    dateAdded_local     = item['dateAdded_local']
    wind_average_deg    = item['wind_average_deg']
    wind_average_dir    = item['wind_average_dir']
    wind_speed_kph      = item['wind_speed_kph']
    wind_speed_mph      = item['wind_speed_mph']
    wind_gust_kph       = item['wind_gust_kph']
    wind_gust_mph       = item['wind_gust_mph']
    rainfall_mm         = item['rainfall_mm']
    rainfall_in         = item['rainfall_in']
    humidity_RA         = item['humidity_RA']
    pressure_hPA        = item['pressure_hPA']
    pressure_inHG       = item['pressure_inHG']
    ambient_temp_c      = item['ambient_temp_c']
    ambient_temp_f      = item['ambient_temp_f']
    ground_temp_c       = item['ground_temp_c']
    ground_temp_f       = item['ground_temp_f']
    dateAdded_utc       = item['dateAdded_utc']
    
    entry_date = dateAdded_local.split()[0]

    if entry_date not in myDates:
        myDates[entry_date] = collections.defaultdict(list)
    myDates[entry_date]["wind_average_deg"].append(wind_average_deg)
    myDates[entry_date]["wind_speed_kph"].append(wind_speed_kph)
    myDates[entry_date]["wind_speed_mph"].append(wind_speed_mph)
    myDates[entry_date]["wind_gust_kph"].append(wind_gust_kph)
    myDates[entry_date]["wind_gust_mph"].append(wind_gust_mph)
    myDates[entry_date]["rainfall_mm"].append(rainfall_mm)
    myDates[entry_date]["rainfall_in"].append(rainfall_in)
    myDates[entry_date]["humidity_RA"].append(humidity_RA)
    myDates[entry_date]["pressure_hPA"].append(pressure_hPA)
    myDates[entry_date]["pressure_inHG"].append(pressure_inHG)
    myDates[entry_date]["ambient_temp_c"].append(ambient_temp_c)
    myDates[entry_date]["ambient_temp_f"].append(ambient_temp_f)
    myDates[entry_date]["ground_temp_c"].append(ground_temp_c)
    myDates[entry_date]["ground_temp_f"].append(ground_temp_f)

for date in myDates:
    if date not in myDatesSum:
        myDatesSum[date] = collections.defaultdict(list)
    myDatesSum[date]['sum_temp_high_c'] = round(max(myDates[date]["ambient_temp_c"]),2)
    myDatesSum[date]['sum_temp_low_c']  = round(min(myDates[date]["ambient_temp_c"]),2)
    myDatesSum[date]['sum_temp_high_f']  = round(max(myDates[date]["ambient_temp_f"]),2)
    myDatesSum[date]['sum_temp_low_f']  = round(min(myDates[date]["ambient_temp_f"]),2)
    myDatesSum[date]['sum_gnd_temp_high_c']  = round(max(myDates[date]["ground_temp_c"]),2)
    myDatesSum[date]['sum_gnd_temp_low_c']  = round(min(myDates[date]["ground_temp_c"]),2)
    myDatesSum[date]['sum_gnd_temp_high_f']  = round(max(myDates[date]["ground_temp_f"]),2)
    myDatesSum[date]['sum_gnd_temp_low_f']  = round(min(myDates[date]["ground_temp_f"]),2)
    myDatesSum[date]['sum_wind_gust_avg_kph'] = round(mean(myDates[date]["wind_gust_kph"]),2)
    myDatesSum[date]['sum_wind_gust_avg_mph'] = round(mean(myDates[date]["wind_gust_mph"]),2)
    myDatesSum[date]['sum_wind_gust_hi_kph'] = round(max(myDates[date]["wind_gust_kph"]),2)
    myDatesSum[date]['sum_wind_gust_hi_mph'] = round(max(myDates[date]["wind_gust_mph"]),2)
    myDatesSum[date]['sum_wind_speed_avg_kph'] = round(mean(myDates[date]["wind_speed_kph"]),2)
    myDatesSum[date]['sum_wind_speed_avg_mph']= round(mean(myDates[date]["wind_speed_mph"]),2)
    myDatesSum[date]['sum_wind_dir_avg_deg'] = round(mean(myDates[date]["wind_average_deg"]),2)
    myDatesSum[date]['sum_wind_dir_avg_str'] = ""
    myDatesSum[date]['sum_rain_total_mm'] = round(sum(myDates[date]["rainfall_mm"]),2)
    myDatesSum[date]['sum_rain_total_in'] = round(sum(myDates[date]["rainfall_in"]),2)
    myDatesSum[date]['sum_pres_hi_hPA'] = round(max(myDates[date]["pressure_hPA"]),2)
    myDatesSum[date]['sum_pres_lo_hPA'] = round(min(myDates[date]["pressure_hPA"]),2)
    myDatesSum[date]['sum_pres_hi_inHG'] = round(max(myDates[date]["pressure_inHG"]),2)
    myDatesSum[date]['sum_pres_lo_inHG'] = round(min(myDates[date]["pressure_inHG"]),2)
    myDatesSum[date]['sum_pres_avg_inHG'] = round(mean(myDates[date]["pressure_inHG"]),2)
    myDatesSum[date]['sum_humidity_hi_RA'] = round(max(myDates[date]["humidity_RA"]),2)
    myDatesSum[date]['sum_humidity_lo_RA'] = round(min(myDates[date]["humidity_RA"]),2)
    myDatesSum[date]['sum_humidity_avg_RA'] = round(mean(myDates[date]["humidity_RA"]),2)

for i in sorted (myDatesSum.keys()):
    entry = WeatherSum(i,myDatesSum[i]['sum_temp_high_c'],myDatesSum[i]['sum_temp_low_c'],myDatesSum[i]['sum_temp_high_f'],myDatesSum[i]['sum_temp_low_f'],myDatesSum[i]['sum_gnd_temp_high_c'],
                myDatesSum[i]['sum_gnd_temp_low_c'],myDatesSum[i]['sum_gnd_temp_high_f'],myDatesSum[i]['sum_gnd_temp_low_f'],myDatesSum[i]['sum_wind_gust_avg_kph'],myDatesSum[i]['sum_wind_gust_avg_mph'],
                myDatesSum[i]['sum_wind_gust_hi_kph'],myDatesSum[i]['sum_wind_gust_hi_mph'],myDatesSum[i]['sum_wind_speed_avg_kph'],myDatesSum[i]['sum_wind_speed_avg_mph'],myDatesSum[i]['sum_wind_dir_avg_deg'],
                myDatesSum[i]['sum_wind_dir_avg_str'],myDatesSum[i]['sum_rain_total_mm'],myDatesSum[i]['sum_rain_total_in'],myDatesSum[i]['sum_pres_hi_hPA'],myDatesSum[i]['sum_pres_lo_hPA'],myDatesSum[i]['sum_pres_hi_inHG'],
                myDatesSum[i]['sum_pres_lo_inHG'],myDatesSum[i]['sum_pres_avg_inHG'],myDatesSum[i]['sum_humidity_hi_RA'],myDatesSum[i]['sum_humidity_lo_RA'],myDatesSum[i]['sum_humidity_avg_RA'])
    dbs.add(entry)
    dbs.commit()
