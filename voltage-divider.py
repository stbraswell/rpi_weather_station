def voltage_divider(rone,rtwo,vin):
	vOut = (vin * rone) / (rone + rtwo)
	return round(vOut,3)

resistors=[33000,6570,8200,891,1000,688,2200,1410,3900,3140,16000,14120,120000,42120,64900,21880]
volts=[]
for idx,resistance in enumerate(resistors):
	print(idx,voltage_divider(4700,resistance,3.3))
	volts.append(voltage_divider(4700,resistance,3.3))
print volts
